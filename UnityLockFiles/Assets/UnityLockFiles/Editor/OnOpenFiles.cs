﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

public class OnOpenFiles {

    [OnOpenAsset(0)]
    public static bool CheckFileLocked(int instanceID, int line)
    {
        // Find asset importer.
        var assetObj = EditorUtility.InstanceIDToObject(instanceID);
        string assetPath = AssetDatabase.GetAssetOrScenePath(assetObj);
        AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);
        
        // Find file lock for asset.
        FileLock fileLock = FileLock.LoadOrCreateFileLock(assetImporter);

        // If file is locked, display message and let user choose if they want to force unlock a file.
        if (fileLock.IsLocked)
        {
            if (!EditorUtility.DisplayDialog("The file is locked",
                string.Format("The file \" {0} \" is locked by {1}.\n\nDo you want to open it anyways?\n\nIf you make changes to the file it might result in merge errors later.", assetObj.name, fileLock.LockedBy), "Open locked file", "Cancel"))
            {
                // If they cancelled, don't open the file.
                return true; // We handled the file (meaning don't open the file).
            }
            // If user wants to open the file, do so.
        }

        return false; // We didn't handle the file (meaning open the file like usually).
    }
}
