﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class UnityLockFilesSettingsWindow : EditorWindow
{
    private static UnityLockFilesSettings _settings;

    [MenuItem("UnityLockFiles/Settings", false, 0)]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        UnityLockFilesSettingsWindow window =
            EditorWindow.GetWindow(typeof(UnityLockFilesSettingsWindow)) as UnityLockFilesSettingsWindow;
        window.Show();
    }

    private void OnEnable()
    {
        // Try and load settings.
        _settings = UnityLockFilesSettings.LoadSettings();
    }

    void OnGUI()
    {
        GUILayout.Label("User settings", EditorStyles.boldLabel);
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        _settings.Email = EditorGUILayout.TextField("Email", _settings.Email);
        EditorGUILayout.HelpBox("The email is used to distinguis between who has locked a file and who hasn't.", MessageType.Info);

        // Auto save if anything has changed!
        if (GUI.changed)
        {
            _settings.Save();
        }
    }
}