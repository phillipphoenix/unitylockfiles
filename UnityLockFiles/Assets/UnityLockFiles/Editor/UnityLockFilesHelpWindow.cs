﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;

public class UnityLockFilesHelpWindow : EditorWindow
{

    private const string Version = "0.0.1";
    private static readonly DateTime _lastUpdated = new DateTime(2016, 9, 8);

    private static Vector2 _scrollPos;

    [MenuItem("UnityLockFiles/Help", false, 1000)]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        UnityLockFilesHelpWindow window =
            EditorWindow.GetWindow(typeof(UnityLockFilesHelpWindow)) as UnityLockFilesHelpWindow;
        window.Show();
        EditorStyles.label.wordWrap = true;
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Unity Lock Files", EditorStyles.boldLabel);
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Version: " + Version + " - Last updated: " + _lastUpdated);
        GUILayout.Space(20);

        EditorGUILayout.LabelField("Help guide", EditorStyles.boldLabel);
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Setup", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("In order to set up the project, please go to Edit->Project settings->Editor and set version control mode to \"Visible meta files\".");
        GUILayout.Space(5);
        EditorGUILayout.LabelField("File states", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Files can be in 3 different states: Unlocked, Locked and LockedByMe.");
        EditorGUILayout.LabelField("When a file is unlocked, it works just like files usually do in Unity. When you double click it, it opens.");
        GUILayout.Label(Resources.Load<Texture2D>("FileNotLocked"));
        EditorGUILayout.LabelField("When a file is locked, a red lock will be displayed over the file and a warning will be displayed, when double clicking it.\nThe file can still be opened, but editing is discouraged.");
        GUILayout.Label(Resources.Load<Texture2D>("LockedFile"));
        EditorGUILayout.LabelField("When a file is lockedByMe, a green lock will be displayed over the file. It means that you have locked the file and others will now see the red lock on the same file, indicating that someone else has the lock.");
        GUILayout.Label(Resources.Load<Texture2D>("LockedByMeFile"));
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Locking and unlocking files", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("In order to lock a file and synchronise it with the rest of the team, you right click a file (or several files) and click on the lock file context menu.");
        GUILayout.Label(Resources.Load<Texture2D>("ContextMenu"));
        EditorGUILayout.LabelField("When the file is locked, you have to commit the meta file of the locked file via version control, for instance git. This will share the lock with the rest of the team.");
        EditorGUILayout.LabelField("This also means that you should pull from your remote repository before starting your work, so you can receive other team members' locks.");
        EditorGUILayout.LabelField("To unlock a file the same procedure can be used. Unlock a file from the context menu and then commit and push the meta file.");
        GUILayout.Space(10);
        EditorGUILayout.EndScrollView();
    }
}
