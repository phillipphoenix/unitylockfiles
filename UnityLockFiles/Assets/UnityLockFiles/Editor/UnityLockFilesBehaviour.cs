﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;

public class UnityLockFilesBehaviour {

    [MenuItem("Assets/Lock file(s)", true)]
    private static bool CanLockFile()
    {
        var instanceIds = Selection.instanceIDs;
        foreach (var instanceId in instanceIds)
        {
            // Can't lock folders, so return false, if one of selected items is a folder.
            if (IsFolder(instanceId)) return false;
        }
        // If no folders, return true.
        return true;
    }

    [MenuItem("Assets/Lock file(s)")]
    private static void LockFile()
    {
        // If no email has been set, ask them to do that first before locking.
        if (string.IsNullOrEmpty(UnityLockFilesSettings.LoadSettings().Email))
        {
            EditorUtility.DisplayDialog("Information needed!",
                "We need some information before we can lock the file!\n\nWe'll open up the settings for you (also found under the UnityLockFiles menu).",
                "Ok");
            (EditorWindow.GetWindow(typeof(UnityLockFilesSettingsWindow)) as UnityLockFilesSettingsWindow).Show();
            return;
        }

        // If they have, continue.
        var activeObjects = Selection.objects;
        foreach (var assetObj in activeObjects)
        {
            string assetPath = AssetDatabase.GetAssetOrScenePath(assetObj);
            AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);

            FileLock fileLock = FileLock.LoadOrCreateFileLock(assetImporter);

            if (fileLock.IsLocked)
            {
                if (!EditorUtility.DisplayDialog("The file is locked",
                    string.Format("The file \" {0} \" is locked by {1}.\n\nDo you want to take the lock?\n\nTaking the lock of the file might result in merge errors later.", assetObj.name, fileLock.LockedBy), "Take file lock", "Cancel"))
                {
                    // If they cancelled, don't do anything.
                    return;
                }
            }
            // If the file isn't locked or they didn't cancel, lock file for this person.
            fileLock.Lock();
            fileLock.Save(assetImporter);
        }
    }

    [MenuItem("Assets/Unlock file(s)", true)]
    private static bool CanUnlockFile()
    {
        var instanceIds = Selection.instanceIDs;
        foreach (var instanceId in instanceIds)
        {
            // Can't unlock folders, so return false, if one of selected items is a folder.
            if (IsFolder(instanceId)) return false;
        }
        // If no folders, return true.
        return true;
    }

    [MenuItem("Assets/Unlock file(s)")]
    private static void UnlockFile()
    {
        var activeObjects = Selection.objects;
        foreach (var assetObj in activeObjects)
        {
            string assetPath = AssetDatabase.GetAssetOrScenePath(assetObj);
            AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);

            FileLock fileLock = FileLock.LoadOrCreateFileLock(assetImporter);

            // If the file is not locked, don't do anything.
            if (!fileLock.IsLocked && !fileLock.IsMyLock) return;
            // If the file is locked, but it's mine, unlock, else if locked by others ask to force unlock.
            if (fileLock.IsMyLock || EditorUtility.DisplayDialog("The file is locked",
                    string.Format("The file \" {0} \" is locked by {1}.\n\nDo you want to force unlock it?\n\nForce unlocking the file might result in merge errors later.",
                        assetObj.name, fileLock.LockedBy), "Force unlock file", "Cancel"))
            {
                // If they want to force unlock it, do so.
                fileLock.Unlock();
                fileLock.Save(assetImporter);
            }
        }
    }

    private static bool IsFolder(int instanceId)
    {
        var path = AssetDatabase.GetAssetPath(instanceId);
        return AssetDatabase.IsValidFolder(path);
    }
	
}
