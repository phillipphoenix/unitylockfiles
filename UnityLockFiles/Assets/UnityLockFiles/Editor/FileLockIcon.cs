﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEditor;

[InitializeOnLoad]
public class FileLockIcon
{

    private const float LargeIconSize = 64f;
    private const float MediumIconSize = 32f;
    private static string _lockIconSmallFilePath;
    private static string _lockIconMediumFilePath;
    private static string _lockIconLargeFilePath;
    private static string _lockIconMineSmallFilePath;
    private static string _lockIconMineMediumFilePath;
    private static string _lockIconMineLargeFilePath;

    static FileLockIcon()
    {
        // Other has file lock icons.
        _lockIconSmallFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesLockIconSmall").FirstOrDefault());
        _lockIconMediumFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesLockIconMedium").FirstOrDefault());
        _lockIconLargeFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesLockIconLarge").FirstOrDefault());

        // You have file lock icons.
        _lockIconMineSmallFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesMyLockIconSmall").FirstOrDefault());
        _lockIconMineMediumFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesMyLockIconMedium").FirstOrDefault());
        _lockIconMineLargeFilePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("UnityLockFilesMyLockIconLarge").FirstOrDefault());

        if (_lockIconSmallFilePath == null || _lockIconMediumFilePath == null || _lockIconLargeFilePath == null ||
            _lockIconMineSmallFilePath == null || _lockIconMineMediumFilePath == null || _lockIconMineLargeFilePath == null)
        {
            Debug.LogError("UnityLockFile can't find its lock icons.");
        }
        EditorApplication.projectWindowItemOnGUI += DrawLockIcon;
    }

    static void DrawLockIcon(string guid, Rect rect)
    {
        var assetPath = AssetDatabase.GUIDToAssetPath(guid);

        if (AssetDatabase.IsValidFolder(assetPath)) return;
        
        // Don't draw anything, if file isn't locked or if it isn't my lock!
        AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);
        if (assetImporter == null) return;
        FileLock fileLock = FileLock.LoadOrCreateFileLock(assetImporter);
        if (!fileLock.IsLocked)
        {
            if (!fileLock.IsMyLock)
            {
                return;
            }
        }
        // If the file is locked, continue!

        var isSmall = rect.width > rect.height;
        if (isSmall)
        {
            rect.width = rect.height;
        }
        else
        {
            rect.height = rect.width;
        }
        
        if (rect.width > LargeIconSize)
        {
            var offset = (rect.width - LargeIconSize) / 2f;
            var position = new Rect(rect.x + offset, rect.y + offset, LargeIconSize, LargeIconSize);

            var texture = AssetDatabase.LoadAssetAtPath<Texture>(fileLock.IsMyLock ? _lockIconMineLargeFilePath : _lockIconLargeFilePath);
            if (texture != null)
            {
                GUI.DrawTexture(position, texture);
            }
            else
            {
                GUI.Label(rect, "LOCKED");
            }
        }
        else if (rect.width > MediumIconSize &&  rect.width <= LargeIconSize)
        {
            var offset = (rect.width - MediumIconSize);
            var position = new Rect(rect.x + offset, rect.y + offset, MediumIconSize, MediumIconSize);

            var texture = AssetDatabase.LoadAssetAtPath<Texture>(fileLock.IsMyLock ? _lockIconMineMediumFilePath : _lockIconMediumFilePath);
            if (texture != null)
            {
                GUI.DrawTexture(position, texture);
            }
            else
            {
                GUI.Label(rect, "LOCKED");
            }
        }
        else
        {
            var texture = AssetDatabase.LoadAssetAtPath<Texture>(fileLock.IsMyLock ? _lockIconMineSmallFilePath : _lockIconSmallFilePath);
            if (texture != null)
            {
                GUI.DrawTexture(rect, texture);
            }
            else
            {
                GUI.Label(rect, "LOCKED");
            }
        }
    }
}