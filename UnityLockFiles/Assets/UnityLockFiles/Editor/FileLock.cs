﻿using System;
using UnityEngine;
using System.Collections;
using UnityEditor;

[Serializable]
public class FileLock
{

    public bool IsLocked
    {
        get
        {
            return _isLocked && UnityLockFilesSettings.LoadSettings().Email != LockedBy;
        }
    }

    public bool IsMyLock
    {
        get
        {
            return _isLocked && UnityLockFilesSettings.LoadSettings().Email == LockedBy;
        }
    }

    public string LockedBy;
    [SerializeField]
    private bool _isLocked;

    public static FileLock LoadOrCreateFileLock(AssetImporter assetImporter)
    {
        FileLock fileLock = null;
        if (string.IsNullOrEmpty(assetImporter.userData))
        {
            CreateAndSaveFileLock(assetImporter, out fileLock);
        }
        else
        {
            try
            {
                fileLock = JsonUtility.FromJson<FileLock>(assetImporter.userData);
            }
            catch (Exception)
            {
                // Error occurred during loading of user data... Override old data with new file lock.
                CreateAndSaveFileLock(assetImporter, out fileLock);
            }
        }
        return fileLock;
    }

    private static void CreateAndSaveFileLock(AssetImporter assetImporter, out FileLock fileLock)
    {
        fileLock = new FileLock();
        assetImporter.userData = JsonUtility.ToJson(fileLock);
        assetImporter.SaveAndReimport();
    }

    public void Lock()
    {
        _isLocked = true;
        LockedBy = UnityLockFilesSettings.LoadSettings().Email;
    }

    public void Unlock()
    {
        _isLocked = false;
        LockedBy = "";
    }

    public void Save(AssetImporter assetImporter)
    {
        assetImporter.userData = JsonUtility.ToJson(this);
        assetImporter.SaveAndReimport();
    }

}
