﻿using System;
using UnityEngine;
using System.Collections;
using UnityEditor;

[Serializable]
public class UnityLockFilesSettings : ScriptableObject
{
    private static string _settingsKeyPrefix;
    private static UnityLockFilesSettings _loadedSettings;

    public string Email;

    public void OnEnable()
    {
        _settingsKeyPrefix = PlayerSettings.companyName + "." + PlayerSettings.productName + ".";
    }

    public static UnityLockFilesSettings LoadSettings()
    {
        if (_loadedSettings == null)
        {
            _loadedSettings = CreateSettings();
            _loadedSettings.Email = EditorPrefs.GetString(_settingsKeyPrefix + "email", "");
        }
        return _loadedSettings;
    }

    public UnityLockFilesSettings Save()
    {
        EditorPrefs.SetString(_settingsKeyPrefix + "email", Email);

        return this;
    }

    public static UnityLockFilesSettings CreateSettings()
    {
        return ScriptableObject.CreateInstance<UnityLockFilesSettings>();
    }

}
